import { createServer, Model, JSONAPISerializer, Response } from "miragejs"

export function makeServer({ environment = "development" } = {}) {
  const server = createServer({
    environment,

    models: {
      user: Model,
    },
    serializers: {
      application: JSONAPISerializer
    },
    seeds(server) {
      server.create("user", { name: "aaa", email: "test@gmail.com", password: "qwerty123" })
      server.create("user", { name: "bbbb", email: "test2@gmail.com", password: "qwerty123" })
    },

    routes() {
      this.namespace = "/api"
      this.urlPrefix = "http://localhost:3000"
      this.get("/users")
      // this.get("/auth/login", (schema, request) => {
      //   return schema.user.findBy({ email:})
      // })
      this.post("/auth/login", function (schema, request) {
        const json = JSON.parse(request.requestBody)
        const res = schema.users.findBy({ email: json.email })
        if (json.password === res.password) {
          return this.serialize(res)
        } else {
          return new Response(401, {}, { errors: ["Niepoprawny email lub haslo"] })
        }
      })
      this.post("auth/register", function (schema, request) {
        const json = JSON.parse(request.requestBody)
        if (json.name.length <= 8) return new Response(401, {}, { errors: ["Hasło musi miec wiecej niz 8 znaków"] })
        if (json.password !== json.repPassword) return new Response(401, {}, { errors: ["Hasla nie sa takie same"] })
        if ((schema.users.findBy({ email: json.email }))) return new Response(401, {}, { errors: ["Jest juz uzytkownik o takim emailu"] })
        if ((schema.users.findBy({ email: json.name }))) return new Response(401, {}, { errors: ["Jest juz uzytkownik o takim nicku"] })
        const res = schema.users.create(json)
        return this.serialize(res)
      })
    },
  })

  return server
}