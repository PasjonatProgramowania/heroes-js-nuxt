export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'heroes-nuxt',
    htmlAttrs: {
      lang: 'pl',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  axios: { baseURL: "http://localhost:3000/api" },
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['./assets/styles/main.sass'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['@/plugins/mirage.js'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    '@nuxtjs/composition-api/module',
    '@nuxtjs/style-resources',
    '@nuxt/http'
    // 'nuxt-vite'
  ],
  // serverMiddleware: {
  //   '/api': '~/api'
  // },
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ['@nuxtjs/axios',
    '@nuxtjs/auth-next'
  ],
  middleware: 'auth',
  router: {
    middleware: ['auth']
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}
